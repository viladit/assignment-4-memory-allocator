#include "mem.h"
#include <stdio.h>

void test_successful_allocation() {
    printf("Running test_successful_allocation...\n");
    void *ptr1 = _malloc(16);
    if (ptr1) {
        printf("Allocation successful\n");
        _free(ptr1);
    } else {
        printf("Allocation failed\n");
    }
}

void test_free_single_block() {
    printf("Running test_free_single_block...\n");
    void *ptr1 = _malloc(16);
    void *ptr2 = _malloc(32);
    if (ptr1 && ptr2) {
        _free(ptr1);
        printf("Freed a single block\n");
        _free(ptr2);
    } else {
        printf("Allocation failed\n");
    }
}

void test_free_multiple_blocks() {
    printf("Running test_free_multiple_blocks...\n");
    void *ptr1 = _malloc(16);
    void *ptr2 = _malloc(32);
    void *ptr3 = _malloc(64);
    if (ptr1 && ptr2 && ptr3) {
        _free(ptr1);
        _free(ptr3);
        printf("Freed multiple blocks\n");
        _free(ptr2);
    } else {
        printf("Allocation failed\n");
    }
}

void test_memory_extension() {
    printf("Running test_memory_extension...\n");
    void *ptr1 = _malloc(16);
    void *ptr2 = _malloc(32);
    if (ptr1 && ptr2) {
        _free(ptr1);
        printf("Freed a block\n");
        void *ptr3 = _malloc(64);
        if (ptr3) {
            printf("Allocated in the extended memory region\n");
            _free(ptr2);
            _free(ptr3);
        } else {
            printf("Allocation failed\n");
            _free(ptr2);
        }
    } else {
        printf("Allocation failed\n");
    }
}

void test_memory_allocation_failure() {
    printf("Running test_memory_allocation_failure...\n");
    void *ptr1 = _malloc(16);
    void *ptr2 = _malloc(32);
    if (ptr1 && ptr2) {
        _free(ptr1);
        _free(ptr2);
        printf("Freed blocks\n");
        void *ptr3 = _malloc(1024 * 1024 * 1024);  // Try to allocate more memory than available
        if (!ptr3) {
            printf("Allocation failure as expected\n");
        } else {
            printf("Unexpected allocation success\n");
            _free(ptr3);
        }
    } else {
        printf("Allocation failed\n");
    }
}

int main() {
    test_successful_allocation();
    test_free_single_block();
    test_free_multiple_blocks();
    test_memory_extension();
    test_memory_allocation_failure();

    return 0;
}
